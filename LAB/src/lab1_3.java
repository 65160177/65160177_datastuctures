import java.util.Arrays;

public class lab1_3 {
    public static void mergesortedArray() { 
        int nums1[] = { 1, 2, 3, 0, 0, 0 };
        int m = 3;
        int nums2[] = { 2, 5, 6 };
        int n = 3;
        System.out.println("Ex.1");
        System.out.println("input: nums1[] = {1,2,3,0,0,0} m = 3, nums2[] = {2,5,6} n = 3");
        for (int i = 0; i < m + n; i++) {
            int n1 = nums1[i];
            int n2 = 0;
            if (i > n) {
                n2 = nums2[i - 3];
            } else if(n != 0){
                n2 = nums2[i];
            }
            if(n == 0){
                break;
            }else if(m == 0){
                nums1 = nums2;
            }else{
            if (nums1[i] < n2) {
                for (int j = nums1.length; j > i + 2; j--) {
                    nums1[j - 1] = nums1[j - 2];
                    
                }
                if (m == 0) {
                    nums1[i] = n2;
                    i += 1;
                } else {
                    nums1[i + 1] = n2;
                    i += 1;
                }

            }
            }
        System.out.print("Output: ");
        Arrays.sort(nums1);
        for (int num : nums1) {
            System.out.print(num + " ");
        }

        int nums1_ex2[] = { 1 };
        int m_ex2 = 1;
        int nums2_ex2[] = {};
        int n_ex2 = 0;
        System.out.println();
        System.out.println("Ex.2");
        System.out.println("input: nums1[] = {1} m = 1, nums2[] = {} n = 0");
        for (int i2 = 0; i2 < m_ex2 + n_ex2; i2++) {
            int n1_ex2 = nums1_ex2[i2];
            int n2_ex2 = 0;
            if (i2 > n_ex2) {
                n2_ex2 = nums2_ex2[i2 - 3];
            } else if(n_ex2 != 0){
                n2_ex2 = nums2_ex2[i2];
            }
            if(n_ex2 == 0){
                break;
            }else if(m_ex2 == 0){
                nums1_ex2 = nums2_ex2;
            }else{
            if (nums1_ex2[i2] < n2_ex2) {
                for (int j = nums1_ex2.length; j > i2 + 2; j--) {
                    nums1_ex2[j - 1] = nums1_ex2[j - 2];
                    
                }
                if (m_ex2 == 0) {
                    nums1_ex2[i2] = n2_ex2;
                    i2 += 1;
                } else {
                    nums1_ex2[i2 + 1] = n2_ex2;
                    i2 += 1;
                }

            }
            }
        }
        System.out.print("Output: ");
        Arrays.sort(nums1_ex2);
        for (int num : nums1_ex2) {
            System.out.print(num + " ");
        }
        int nums1_ex3[] = { 0 };
        int m_ex3 = 0;
        int nums2_ex3[] = { 1 };
        int n_ex3 = 1;
        System.out.println();
        System.out.println("Ex.3");
        System.out.println("input: nums1[] = {0} m = 0, nums2[] = {1} n = 1");
       for (int i3 = 0; i3 < m_ex3 + n_ex3; i3++) {
            int n1_ex3 = nums1_ex2[i3];
            int n2_ex3 = 0;
            if (i3 > n_ex3) {
                n2_ex3 = nums2_ex3[i3 - 3];
            } else if(n_ex2 != 0){
                n2_ex3 = nums2_ex3[i3];
            }
            if(n_ex3 == 0){
                break;
            }else if(m_ex3 == 0){
                nums1_ex3 = nums2_ex3;
            }else{
            if (nums1_ex3[i3] < n2_ex3) {
                for (int j = nums1_ex3.length; j > i3 + 2; j--) {
                    nums1_ex3[j - 1] = nums1_ex3[j - 2];
                    ;
                }
                if (m_ex2 == 0) {
                    nums1_ex3[i3] = n2_ex3;
                    i3 += 1;
                } else {
                    nums1_ex3[i3 + 1] = n2_ex3;
                    i3 += 1;
                }

            }
            }
        }
        System.out.print("Output: ");
        Arrays.sort(nums1_ex3);
        for (int num : nums1_ex3) {
            System.out.print(num + " ");
        }
    }
    }
public static void main(String[] args) {
    System.out.println("lab 1.3");
    mergesortedArray();

    
}

    
}
