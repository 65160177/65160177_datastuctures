public class Queue {
    public static void main(String[] args) {
        Queue theQueue = new Queue(5); // Queue holds 5 items
        theQueue.insert(10); // Insert 4 items
        theQueue.insert(20);
        theQueue.insert(30);
        theQueue.insert(40);
        theQueue.remove(); // Remove 3 items (10, 20, 30)
        theQueue.remove();
        theQueue.remove();
        theQueue.insert(50); // Insert 4 more items (wraps around)
        theQueue.insert(60);
        theQueue.insert(70);
        theQueue.insert(80);

        while (!theQueue.isEmpty()) {
            // Remove and display all items
            long n = theQueue.remove(); // (40, 50, 60, 70, 80)
            System.out.print(n + " ");
        }
        System.out.println(); // Print a newline after all items are displayed
    } // end main()
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;
    private int nItems;

    public Queue(int s) {
        maxSize = s;
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void insert(long j) {
        if (rear == maxSize - 1) // deal with wraparound
            rear = -1;
        queArray[++rear] = j; // increment rear and insert
        nItems++; // one more item
    }

    public long remove() {
        long temp = queArray[front++]; // get value and incr front
        if (front == maxSize) // deal with wraparound
            front = 0;
        nItems--; // one less item
        return temp;
    }

    public long peekFront() {
        return queArray[front];
    }

    public boolean isEmpty() {
        return (nItems == 0);
    }

    public boolean isFull() {
        return (nItems == maxSize);
    }

    public int size() {
        return nItems;
    }
}